﻿#include <iostream>

using namespace std;

class Vector
{
public:
	Vector() : x(10), y(10), z(0)
	{}
	
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}

	void Show()
	{
		cout << x << " " << y << " " << z << " " << endl;
	}

	void VectorLength()
	{
		

		cout << "Length of your vector: " << sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2)) << endl;



	}

private:

	double x;
	double y;
	double z;
};


int main()
{
    
	Vector v(1, 3 , 5), v1, v2(1, 10, 0);
	
	v.Show();
	v1.Show();
	v2.Show();


	v.VectorLength();

	v1.VectorLength();

	v2.VectorLength();

	



}



